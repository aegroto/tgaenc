#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <limits.h>
#include <math.h>

#include "utils.h"
#include "imgops.h"
#include "runlength.h"
#include "console_codes.h"

#define DEBUG 0

#define MAX_IMG_SIZE 4096*4096

#define rand_float() rand() * 1.0 / RAND_MAX

typedef struct {
    cl_kernel kernel;
    size_t preferred_wg; 
} cl_kernel_data_t;

struct {
 	cl_platform_id p;
	cl_device_id d;
	cl_context ctx;
	cl_command_queue que;
	cl_program prog;

    cl_kernel_data_t rl_encode_k;
    cl_kernel_data_t rl_compact_k;
} CL_VARS;

void init_cl_vars();
void destroy_cl_vars();
int encode_image(char* filename, cl_mem d_input, cl_mem d_output);
int encode_database_test(cl_mem d_input, cl_mem d_output);

int random_input_rltest(int n_tests, size_t length, double variance, unsigned max_value, cl_mem d_input, cl_mem d_output);

int main(int argc, char** argv) {
    srand(time(0));

    init_cl_vars();

    // create ocl buffers
    cl_int err;

    size_t rl_input_memsize = MAX_IMG_SIZE * sizeof(unsigned int);
    size_t rl_output_memsize = MAX_RUNLENGTH_SIZE(rl_input_memsize) + 1;

    cl_mem d_input = clCreateBuffer(CL_VARS.ctx,
		CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY,
		rl_input_memsize, NULL, &err);
	ocl_check(err, "create buffer d_input");

    cl_mem d_output = clCreateBuffer(CL_VARS.ctx,
		CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY,
		rl_output_memsize, NULL, &err);
	ocl_check(err, "create buffer d_output");

    // random input tests
    random_input_rltest(1, 1024*1024*2, 0.3, INT_MAX / 1024, d_input, d_output);

    // TGA encoding test
    // encode_database_test(d_input, d_output);

    // encode_image("3_6x4", d_input, d_output);

    // testing: reading output file
    /*unsigned char *output_data = NULL;
    size_t output_size = load_raw_bytes("output_images/7_4x4.tga", &output_data);

    if(output_size == 0) {
        perror("error reading output image");
        return -1;
    }

    // print out image pixels
    printf("Output: ");
    for(int i = 0; i < output_size; ++i) {
        printf("%s ", uchar_to_binstr(output_data[i]));
    }
    printf("\n\n");*/

    clReleaseMemObject(d_input);    
    clReleaseMemObject(d_output);  

    destroy_cl_vars();

    printf(BOLD GREEN "\n### OK ###\n" RESET);

    return 0;
}

void print_evt_stats(char* evtname, cl_event* clevt, unsigned int numpixels) {
    double runtime = runtime_ms(*clevt);

    printf(BRIGHTGREEN " --- %18s: \t" RESET "%u pixels in %.8fms: \t" BRIGHTCYAN "%f" RESET " gigapixels/sec\n",
        evtname, numpixels, runtime, (numpixels / runtime) / 1024.0);
}

void print_total_stats(cl_event *evt_write, cl_event *evt_rlencode, cl_event *evt_read, cl_event *evt_compacts, unsigned int numcompacts, unsigned int numpixels) {
    double runtime = 0.0;

    runtime += runtime_ms(*evt_write);
    runtime += runtime_ms(*evt_rlencode);
    runtime += runtime_ms(*evt_read);

    for(unsigned int i = 0; i < numcompacts; ++i) {
        runtime += runtime_ms(evt_compacts[i]);
    }

    printf(BRIGHTMAGENTA " --- Final stats: " RESET "%u pixels in %.8fms, " BOLD BRIGHTCYAN "%f" RESET " gigapixels/sec\n", numpixels, runtime, (numpixels / runtime) / 1024.0);
}

size_t rl_encode_and_compact_cl(unsigned int* input, rl_entry_t* encoded_input, cl_mem d_input, cl_mem d_output, size_t length) {
#if 1
    cl_int err;

    // copy image data into input buffer
    cl_event evt_write;
    err = clEnqueueWriteBuffer(CL_VARS.que, d_input, CL_TRUE, 0, length * 4, (unsigned char*) input, 0, NULL, &evt_write);
    ocl_check(err, "write buffer");

    // launch encode kernel
    cl_kernel_data_t rl_encode_kernel = CL_VARS.rl_encode_k;
    int elms_per_kernel = (length + CL_RUNLENGTH_ELEMENTS_CHUNK_FACTOR - 1) / CL_RUNLENGTH_ELEMENTS_CHUNK_FACTOR; // CL_RUNLENGTH_ELEMENTS_CHUNK_SIZE;
    printf("elms per kernel: %d\n", elms_per_kernel);

    cl_event evt_rlencode = rl_encode_cl(CL_VARS.que, rl_encode_kernel.kernel, d_input, d_output, length, rl_encode_kernel.preferred_wg, elms_per_kernel, 0, NULL);

    err = clFinish(CL_VARS.que);
    ocl_check(err, "clFinish");

    size_t length_up = length + (length % elms_per_kernel);

    size_t current_size = elms_per_kernel;
    size_t rl_img_entries = MAX_RUNLENGTH_SIZE(length_up) / 2;

    // stream compaction
    cl_kernel_data_t rl_compact_kernel = CL_VARS.rl_compact_k;

    unsigned int numrlcompacts = (unsigned int) log2(rl_img_entries / elms_per_kernel);
    cl_event evt_rlcompacts[numrlcompacts];
    unsigned int evt_rlc_lastindex = 0;

#if 1
    while(current_size < rl_img_entries) {
        /*cl_event evt_read;
        err = clEnqueueReadBuffer(CL_VARS.que, d_output, CL_TRUE, 0, MAX_RUNLENGTH_SIZE(length_up) * 4, (unsigned char*) encoded_input, 0, NULL, &evt_read);
        ocl_check(err, "read buffer");

        printf("\nEncoded input:\n");
        int i = 0;
        // while(encoded_input[i].run_length != 0) {
        while(i < MAX_RUNLENGTH_SIZE(length)) {
            printf("%u %d\n", encoded_input[i].pixel, encoded_input[i].run_length);
            ++i;
        }
        printf("\n");*/

        evt_rlcompacts[evt_rlc_lastindex] = rl_stream_compaction_cl(CL_VARS.que, rl_compact_kernel.kernel, d_output, rl_img_entries * 2, rl_compact_kernel.preferred_wg, current_size, 0, NULL);

        err = clFinish(CL_VARS.que);
        ocl_check(err, "clFinish before compact");

        current_size *= 2;
        ++evt_rlc_lastindex;
    }
#endif

    // load output data into host
    cl_event evt_read;
    err = clEnqueueReadBuffer(CL_VARS.que, d_output, CL_TRUE, 0, MAX_RUNLENGTH_SIZE(length_up) * 4, (unsigned char*) encoded_input, 0, NULL, &evt_read);
    ocl_check(err, "read buffer");

    err = clFinish(CL_VARS.que);
    ocl_check(err, "clFinish");

    size_t rl_img_size = 0;

    while(rl_img_size < length && encoded_input[rl_img_size].run_length != 0) {
        ++rl_img_size;
    }

    print_evt_stats("write", &evt_write, length);
    print_evt_stats("rl_encode_cl", &evt_rlencode, length);
    for(int i = 0; i < evt_rlc_lastindex; ++i) {
        print_evt_stats("rl_compact_cl", &evt_rlcompacts[i], current_size * 2);
    }
    print_evt_stats("read", &evt_read, length);
#else
    size_t rl_img_size = 0;

    while(rl_img_size < MAX_RUNLENGTH_SIZE(length) && encoded_input[rl_img_size].run_length != 0) {
        ++rl_img_size;
    }
#endif

    printf("\n");
    print_total_stats(&evt_write, &evt_rlencode, &evt_read, evt_rlcompacts, numrlcompacts, length);

    return rl_img_size;
}

int random_input_rltest(int n_tests, size_t length, double variance, unsigned int max_value, cl_mem d_input, cl_mem d_output) {
    int test = 0;

    size_t approx_length = length;

    unsigned int *input = (unsigned int*) malloc(approx_length * sizeof(unsigned int));
    memset(input, 0, approx_length * sizeof(unsigned int));

    /*unsigned int _input[] = {
        1792525, 1792525, 1792525, 1792525, 1792525, 1792525, 1679002, 353819, 353819, 353819, 1651023,
    };
    unsigned int *input = (unsigned int*) _input;*/

    rl_entry_t *encoded_input = (rl_entry_t*) malloc(MAX_IMG_SIZE * sizeof(rl_entry_t*));

    unsigned int *output = (unsigned int*) malloc(approx_length * sizeof(unsigned int));

    while(test < n_tests) {
        unsigned int value = rand_float() * max_value; 

        for(size_t i = 0; i < length; ++i) {
            input[i] = value;

            if(rand_float() < variance) {
                value = rand_float() * max_value;
            }
        }

#if DEBUG
        printf("Input (%lu, %lu): ", length, approx_length);
        for(size_t i = 0; i < approx_length; ++i) {
            printf("%u, ", input[i]);
        }
        printf("\n\n");
#endif

        rl_encode_and_compact_cl(input, encoded_input, d_input, d_output, approx_length);

#if DEBUG
        printf("\nEncoded input:\n");
        int i = 0;
        // while(encoded_input[i].run_length != 0) {
        while(i < MAX_RUNLENGTH_SIZE(length)) {
            printf("%u %d\n", encoded_input[i].pixel, encoded_input[i].run_length);
            ++i;
        }
        printf("\n");
#endif
        
        size_t output_length = rl_decode_serial(encoded_input, output);

#if DEBUG
        printf("Output: ");
        for(size_t i = 0; i < output_length; ++i) {
            printf("%u ", output[i]);
        }
        printf("\n");
#endif

        if(length != output_length) {
            printf("WARNING: output has a different length than input. Both size approximation and errors may be the cause.\n");
        }
        
        for(size_t i = 0; i < length; ++i) {
            if(input[i] != output[i]) {
                printf(RED BOLD "RUN-LENGTH ERROR\n" RESET);
                return 1;
            }
        }

        ++test;
    }

    free(input);
    free(encoded_input);
    free(output);

    return 0;
}

void init_cl_vars() {
	CL_VARS.p = select_platform();
	CL_VARS.d = select_device(CL_VARS.p);
	CL_VARS.ctx = create_context(CL_VARS.p, CL_VARS.d);
	CL_VARS.que = create_queue(CL_VARS.ctx, CL_VARS.d);
	CL_VARS.prog = create_program("cl_src/runlength.ocl", CL_VARS.ctx, CL_VARS.d);

    cl_int err;

	CL_VARS.rl_encode_k.kernel = clCreateKernel(CL_VARS.prog, "rl_encode", &err);
	ocl_check(err, "create kernel rl_encode");

	err = clGetKernelWorkGroupInfo(CL_VARS.rl_encode_k.kernel, CL_VARS.d,
		CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,
		sizeof(CL_VARS.rl_encode_k.preferred_wg), &CL_VARS.rl_encode_k.preferred_wg,
		NULL);
	ocl_check(err, "get preferred wg size multiple for rl_encode");

	CL_VARS.rl_compact_k.kernel = clCreateKernel(CL_VARS.prog, "rl_compact", &err);
	ocl_check(err, "create kernel rl_compact");

	err = clGetKernelWorkGroupInfo(CL_VARS.rl_compact_k.kernel, CL_VARS.d,
		CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,
		sizeof(CL_VARS.rl_compact_k.preferred_wg), &CL_VARS.rl_compact_k.preferred_wg,
		NULL);
	ocl_check(err, "get preferred wg size multiple for rl_compact");
}

void destroy_cl_vars() {
    clReleaseKernel(CL_VARS.rl_compact_k.kernel);
    clReleaseKernel(CL_VARS.rl_encode_k.kernel);
    clReleaseProgram(CL_VARS.prog);
    clReleaseCommandQueue(CL_VARS.que);
    clReleaseContext(CL_VARS.ctx);
    clReleaseDevice(CL_VARS.d);
}

int encode_image(char* filename, cl_mem d_input, cl_mem d_output) {
    unsigned char* img_data = NULL;
    img_size_t img_size;

    char complete_filename[128];

    sprintf(complete_filename, "input_images/%s.data", filename);

    img_size = load_image_data(complete_filename, &img_data);

    if(img_size.width == 0) {
        perror("error loading image");
        return 1;
    }
    
    size_t img_size_bytes = (img_size.width * img_size.height) * 4;

#if DEBUG
    // print out image pixels
    printf("Image pixels: ");
    for(int i = 0; i < img_size_bytes; ++i) {
        printf("%u ", img_data[i]);
    }
    printf("\n\n");
#endif

    // encode using runlength method
#if 0
    size_t rl_img_max_size = (img_size_bytes / 4) * sizeof(unsigned int) + img_size_bytes * sizeof(unsigned int); 
    unsigned char* rl_img_data = malloc(rl_img_max_size * sizeof(unsigned char));

    rl_encode_serial(img_data, img_size_bytes, rl_img_data);

    rl_entry_t* rl_img_data_entries = (rl_entry_t*) rl_img_data;
#endif

#if 1
    size_t numels = img_size_bytes / 4;

    size_t approx_numels = numels; // next_power_of_two(numels);

    rl_entry_t *rl_img_data = malloc(numels * sizeof(rl_entry_t));

    size_t rl_img_size = rl_encode_and_compact_cl((unsigned int*) img_data, rl_img_data, d_input, d_output, approx_numels);
#endif

#if DEBUG
    // print out image encoded pixels
    printf("\nEncoded pixels (%lu):\n", rl_img_size);
    int* rl_img_data_pixels = (int*) rl_img_data;
    for(int i = 0; i < rl_img_size * 2; i += 2) {
        printf("%s %d\n", uint_to_binstr(rl_img_data_pixels[i]), rl_img_data_pixels[i + 1]);
    }
    printf("\n\n");
#endif

    sprintf(complete_filename, "output_images/%s.tga", filename);

    if(write_tga_image(complete_filename, (unsigned char*) rl_img_data, img_size.width, img_size.height, rl_img_size * 8) != 0) {
        perror("error writing tga image");
        return -1;
    }

#if DEBUG 
    // testing: reading output file
    unsigned char *output_data = NULL;
    size_t output_size = load_raw_bytes(complete_filename, &output_data);

    if(output_size == 0) {
        perror("error reading output image");
        return -1;
    }

    // print out image pixels
    printf("Output: ");
    for(int i = 0; i < output_size; ++i) {
        printf("%s ", uchar_to_binstr(output_data[i]));
    }
    printf("\n\n");
#endif

    return 0;
}

int encode_database_test(cl_mem d_input, cl_mem d_output) {
    // parse directory "input_images" and encode every file
    DIR* dir;
    struct dirent *dirent;

    dir = opendir("./input_images/");
    if(dir == NULL) {
        perror("error opening dir");
        return 1;
    }

    while((dirent = readdir(dir)) != NULL) {
        if(dirent->d_name[0] != '.') {
            char *filename = strtok(dirent->d_name, ".");
            printf(BOLD BRIGHTMAGENTA " ### Encoding \"%s\"...\n" RESET, filename);
            encode_image(filename, d_input, d_output);
            printf(BOLD BRIGHTMAGENTA " ### Encoded \"%s\"\n\n" RESET, filename);
        }
    }

    return 0;
}