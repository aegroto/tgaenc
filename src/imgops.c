#include "imgops.h"

typedef struct {
    int run_length;
    int pixel;
} tga_rl_entry_t;

size_t adapt_rl_to_tga(unsigned char *img_data, size_t data_bytes, size_t row_length, tga_rl_entry_t **out_img_data, size_t max_output_pixels);
unsigned int get_pixel_to_save(unsigned int last_value);

img_size_t load_image_data(char *path, unsigned char** out_img_data) {
    FILE *fp;
    img_size_t img_size;
    memset(&img_size, 0, sizeof(img_size));

    if((fp = fopen(path, "r")) == NULL) {
        return img_size;
    }

    const char *filename = basename(path);

    char *remaining_filename = strdup(filename);

    char *token = strtok(remaining_filename, "_");
    token = strtok(NULL, "x");
    img_size.width = atoi(token);
    token = strtok(NULL, ".");
    img_size.height = atoi(token);

    size_t img_size_bytes = (img_size.width * img_size.height) * 4;

    unsigned char* img_data = malloc(sizeof(unsigned char) * img_size_bytes);

    int total_read_bytes = 0, read_bytes = 0;

    while(total_read_bytes < img_size_bytes) {
        read_bytes = fread(img_data + total_read_bytes, sizeof(char), img_size_bytes, fp); 

        if(read_bytes == 0) {
            printf("WARNING: not enough data read from image (%d/%lu)", total_read_bytes, img_size_bytes);
            break;
        }

        total_read_bytes += read_bytes;
    }

    fclose(fp);

    *out_img_data = img_data;

    return img_size;
}

int write_tga_image(char *path, unsigned char* img_data, size_t width, size_t height, size_t data_bytes) {
    FILE *fp;

    if((fp = fopen(path, "w+")) == NULL) {
        return -1;
    }

    // Header encoding 

    TGA_HEADER header;

    memset(&header, 0, sizeof(header));

    header.idlength = 0;
    header.colourmaptype = 0;
    header.datatypecode = 10;

    header.x_origin = 0;
    header.y_origin = 0;

    header.width = width;
    header.height = height;

    header.imagedescriptor |= 8;

    header.bitsperpixel = 32;

    if(fwrite(&header, sizeof(header), 1, fp) == 0) {
        perror("error writing header");
        return -1;
    }

    // Image data encoding

    // unsigned int* img_data_pixels = (unsigned int*) img_data;
    tga_rl_entry_t *img_data_pixels = NULL;
    size_t data_entries = adapt_rl_to_tga(img_data, data_bytes, width, &img_data_pixels, width * height);

    // size_t data_ints = data_bytes / 4;
    long i = 0;
    // long x = 0, y = data_ints - 1;

    while(i < data_entries) {
        // int run_length = img_data_pixels[i];        
        // int pixel = img_data_pixels[i + 1];
        tga_rl_entry_t entry = img_data_pixels[i];

        i++;
        
        // printf("pixel: %s, run length: %u", uint_to_binstr(entry.pixel), (unsigned int) (entry.run_length));
        // printf(" (%s)\n", uint_to_binstr((unsigned int) entry.run_length));

        while(entry.run_length > 0) {
            // unsigned char current_run_length = ((unsigned char) entry.run_length - 1) & 0b01111111;
            unsigned char current_run_length = entry.run_length > 128 ? 128 : entry.run_length;

            // printf("pixel: %s, current run length: %u/%u (%s)\n", uint_to_binstr(entry.pixel), current_run_length, entry.run_length, uchar_to_binstr(current_run_length));

            unsigned char packet_header = (current_run_length - 1) | 0b10000000;

            fwrite(&packet_header, sizeof(char), 1, fp);
            fwrite(&entry.pixel, sizeof(int), 1, fp);

            entry.run_length -= 128;
        }
    }

    fclose(fp);

    return 0;
}

unsigned int get_pixel_to_save(unsigned int last_value) {
    unsigned char a = (last_value >> 24) & 0xFF;
    unsigned char b = (last_value >> 16) & 0xFF;
    unsigned char g = (last_value >> 8) & 0xFF;
    unsigned char r = (last_value     ) & 0xFF;

#if 0 
    printf("last value >> 24: %s\n", uint_to_binstr((last_value >> 24)));
    printf("last value >> 16: %s\n", uint_to_binstr((last_value >> 16)));
    printf("last value >> 8: %s\n", uint_to_binstr((last_value >> 8)));
    printf("last value: %s\n", uint_to_binstr((last_value)));
    printf("channel r: %s\n", uchar_to_binstr(r));
    printf("channel g: %s\n", uchar_to_binstr(g));
    printf("channel b: %s\n", uchar_to_binstr(b));
    printf("channel a: %s\n\n", uchar_to_binstr(a));
#endif

    unsigned int pixel_to_save = b | g << 8 | r << 16 | a << 24;

    return pixel_to_save;
}

size_t adapt_rl_to_tga(unsigned char *img_data, size_t data_bytes, size_t row_length, tga_rl_entry_t **out_img_data, size_t max_output_pixels) {
    size_t data_ints = data_bytes / 4;

    unsigned int *in_pixels = (unsigned int*) img_data;
    tga_rl_entry_t *out_pixels = (tga_rl_entry_t*) malloc(max_output_pixels * sizeof(tga_rl_entry_t));

    // first, convert unsigned int pairs to tga_rl_entry_t and invert entries order
    int i = data_ints - 1;
    int o = 0;
    tga_rl_entry_t current_entry;
    memset(&current_entry, 0, sizeof(current_entry));

    int remaining_row_pixels = row_length;

    while(i > 0) {
        // if the current run-length has been completely written, jump to the next one
        if(current_entry.run_length <= 0) {
            current_entry.run_length = in_pixels[i];
            current_entry.pixel = in_pixels[i - 1];

            i -= 2;
        }

        out_pixels[o].pixel = get_pixel_to_save(current_entry.pixel);

        /* if there aren't enough pixels remaining in the current row for the next run-length
           or the row is gonna get filled by the latter put as much as pixels as possible in 
           the remaining space and go to the next row.
           otherwise, keep filling the current row.
        */

        if(remaining_row_pixels <= current_entry.run_length) {
            out_pixels[o].run_length = remaining_row_pixels;
            current_entry.run_length -= remaining_row_pixels;

            remaining_row_pixels = row_length;
        } else {
            out_pixels[o].run_length = current_entry.run_length;

            remaining_row_pixels -= current_entry.run_length;
            current_entry.run_length = 0;
        }
        
        ++o;
    }

    // now reverse the order of pixels in every row.
    int p = 0, m = 0;
    size_t current_row_length = 0;

    while(p < o) {
        current_row_length += out_pixels[p].run_length;

        // if we fulfilled a row, we swap every pixel with its opposite
        // this is due to the fact that pixels should be encoded in their 
        // current order, while rows should be encoded in reverse order.
        if(current_row_length >= row_length) {
            int s = 0;

            while(m + s < p - s) {
                tga_rl_entry_t tmp = out_pixels[m + s];
                out_pixels[m + s] = out_pixels[p - s];
                out_pixels[p - s] = tmp;
                ++s;
            }

            m = p + 1;
            current_row_length = 0;
        }

        ++p;
    }

    // set the pointer to output data
    *out_img_data = out_pixels;

    return o;
}

size_t load_raw_bytes(char *path, unsigned char** out_data) {
    FILE *fp;

    if((fp = fopen(path, "r")) == NULL) {
        return 0;
    }

    fseek(fp, 0L, SEEK_END);
    size_t size = ftell(fp);
    rewind(fp);
    
    unsigned char* data = malloc(sizeof(unsigned char) * size);

    size_t read_bytes = 0, total_read_bytes = 0;

    while(1) {
        read_bytes = fread(data + total_read_bytes, sizeof(unsigned char), size, fp); 

        if(read_bytes == 0)
            break;

        total_read_bytes += read_bytes;
    }

    fclose(fp);

    *out_data = data;

    return size;
}