#include "runlength.h"

size_t rl_encode_serial(unsigned char *input, size_t input_size, unsigned char *output) {
    // encode the pixels, so TGA conversion is easier
    unsigned int* input_pixels = (unsigned int*) input;
    unsigned int* output_pixels = (unsigned int*) output;

    size_t input_size_pixels = input_size / 4;

    unsigned int last_value = input_pixels[0];
    unsigned int current_run_length = 1;

    int i = 1, o = 0;

    while(i < input_size_pixels) {
        unsigned int value = input_pixels[i];

        if(value != last_value) {
            output_pixels[o++] = last_value;
            output_pixels[o++] = current_run_length;

            last_value = value;
            current_run_length = 0;
        }

        ++current_run_length;

        ++i;
    }

    output_pixels[o++] = last_value;
    output_pixels[o++] = current_run_length;

    output_pixels[o + 1] = 0;

    return o * 4;
}

size_t rl_decode_serial(rl_entry_t *encoded_input, unsigned int *output) {
    size_t i = 0, o = 0;

    while(encoded_input[i].run_length > 0) {
        rl_entry_t current_entry = encoded_input[i];

        while(current_entry.run_length > 0) {
            output[o++] = current_entry.pixel;
            --current_entry.run_length;
        }

        ++i;
    }

    return o;
}

cl_event rl_encode_cl(
    cl_command_queue que, cl_kernel rl_encode_cl_k,
	cl_mem d_input, cl_mem d_output, cl_int numels, size_t wg_preferred, int elms_per_kernel,
	int n_wait_events, cl_event *wait_events
) {
	cl_int err;
	err = clSetKernelArg(rl_encode_cl_k, 0, sizeof(d_input), &d_input);
	ocl_check(err, "set rl_encode_cl arg 0");
	err = clSetKernelArg(rl_encode_cl_k, 1, sizeof(d_output), &d_output);
	ocl_check(err, "set rl_encode_cl arg 1");
	err = clSetKernelArg(rl_encode_cl_k, 2, sizeof(numels), &numels);
	ocl_check(err, "set rl_encode_cl arg 2");
	err = clSetKernelArg(rl_encode_cl_k, 3, sizeof(elms_per_kernel), &elms_per_kernel);
	ocl_check(err, "set rl_encode_cl arg 3");

	const size_t gws[] = { round_mul_up(numels, wg_preferred) };
	printf("rl_encode_cl_k: gws %d | %zu => %zu\n",
		numels, wg_preferred, gws[0]);

	cl_event evt_encode;
	err = clEnqueueNDRangeKernel(
		que, rl_encode_cl_k,
		1, NULL, gws, NULL,
		n_wait_events, wait_events, &evt_encode);
	ocl_check(err, "enqueue rl_encode_cl_k");

	return evt_encode;
}

cl_event rl_stream_compaction_cl(cl_command_queue que, cl_kernel rl_compact_cl_k, cl_mem d_input, cl_int numels, size_t wg_preferred, int entries_per_subarray, int n_wait_events, cl_event *wait_events) {
	cl_int err;

	err = clSetKernelArg(rl_compact_cl_k, 0, sizeof(d_input), &d_input);
	ocl_check(err, "set rl_compact_cl arg 0");
	err = clSetKernelArg(rl_compact_cl_k, 1, sizeof(numels), &numels);
	ocl_check(err, "set rl_compact_cl arg 1");
	err = clSetKernelArg(rl_compact_cl_k, 2, sizeof(entries_per_subarray), &entries_per_subarray);
	ocl_check(err, "set rl_compact_cl arg 2");

	const size_t gws[] = { round_mul_up(numels, wg_preferred) };
	// printf("rl_compact_cl_k: gws %d | %zu => %zu\n", numels, wg_preferred, gws[0]);

	cl_event evt_init;
	err = clEnqueueNDRangeKernel(
		que, rl_compact_cl_k,
		1, NULL, gws, NULL,
		n_wait_events, wait_events, &evt_init);
	ocl_check(err, "enqueue rl_compact_cl_k");

	return evt_init;
}

size_t rl_stream_compaction_serial(rl_entry_t *input, size_t i, size_t m, size_t j) {
    rl_entry_t current_entry = input[m - 1];

    // if the first half is a completely filled, then the stream is already compacted
    if(current_entry.run_length != 0) {
        // printf("left stream is full : %d\n", current_entry.run_length);
        return j - i + 1;
    }


    // find first left free entry.
    size_t l = i;
    while(input[l].run_length != 0) {
        ++l;
    }

    // printf("l: %lu\n", l);

    size_t index_diff = m - l;
    size_t n = m;

    // printf("index_diff: %lu\n", index_diff);
    // printf("n: %lu\n", n);

    current_entry = input[n];
    while(n <= j && current_entry.run_length != 0) {
        // printf("moving entry: %s %d\n", uint_to_binstr(current_entry.pixel), current_entry.run_length);

        input[n - index_diff] = current_entry;

        ++n;
        current_entry = input[n];
    }

    input[n - index_diff].run_length = 0;
    input[j].run_length = 0;

    return j - i + 1;
}