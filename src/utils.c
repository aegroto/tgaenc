#include "utils.h"

char* uchar_to_binstr(unsigned char input) {
    static char output[8 + 1];

    char tmp = input;
    int i = 0;

    while(i < 8){
        output[7 - i] = tmp % 2 ? '1' : '0';

        tmp >>= 1;
        ++i;
    }

    output[8] = 0;

    return output;
}

char* uint_to_binstr(unsigned int input) {
    static char output[32 + 1];

    unsigned int tmp = input;
    int i = 0;

    while(i < 32){
        output[31 - i] = tmp % 2 ? '1' : '0';

        tmp >>= 1;
        ++i;
    }

    output[32] = 0;

    return output;
}

// source: https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
unsigned int next_power_of_two(unsigned int v) {
    --v;

    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;

    ++v;

    return v;
}