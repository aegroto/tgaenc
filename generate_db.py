import numpy

from io import open
from shutil import rmtree
from os import listdir, mkdir
from PIL import Image

rmtree("input_images")
mkdir("input_images")

for image_file in listdir("img_database"):
    if(image_file[0] != '.'):
        print("Encoding raw data: {}".format(image_file))

        image = Image.open("img_database/"+image_file)
        if image.mode != 'RGBA':
            image = image.convert('RGBA')
        pixels = numpy.asarray(image) 

        outfile = open("input_images/{}_{}x{}.data".format(image_file.split(".")[0].replace("_", "-"), image.width, image.height), "wb")
        outfile.write(pixels)