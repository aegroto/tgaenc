#ifndef IMG_OPS_H
#define IMG_OPS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libgen.h>

#include "utils.h"
#include "runlength.h"

#pragma pack(push,1)
typedef struct {
   char  idlength;
   char  colourmaptype;
   char  datatypecode;
   short int colourmaporigin;
   short int colourmaplength;
   char  colourmapdepth;
   short int x_origin;
   short int y_origin;
   short width;
   short height;
   char  bitsperpixel;
   char  imagedescriptor;
} TGA_HEADER;
#pragma pack(pop)

typedef struct {
    size_t width;
    size_t height;
} img_size_t;

img_size_t load_image_data(char *path, unsigned char** out_img_data); 
int write_tga_image(char *path, unsigned char* img_data, size_t width, size_t height, size_t data_bytes);
size_t load_raw_bytes(char *path, unsigned char** out_data);

#endif