#ifndef RUN_LENGTH_H
#define RUN_LENGTH_H

#define MAX_RUNLENGTH_SIZE(length) length * 2

#define CL_RUNLENGTH_ELEMENTS_CHUNK_FACTOR 4 

#include <stdlib.h>
#include <stdio.h>

#include "utils.h"
#include "ocl_boiler.h"

typedef struct {
    unsigned int pixel;
    int run_length;
} rl_entry_t;

size_t rl_encode_serial(unsigned char *input, size_t input_size, unsigned char *output);
size_t rl_decode_serial(rl_entry_t *encoded_input, unsigned int *output);
cl_event rl_encode_cl(cl_command_queue que, cl_kernel rl_encode_cl_k, cl_mem d_input, cl_mem d_output, cl_int numels, size_t wg_preferred, int elms_per_kernel, int n_wait_events, cl_event *wait_events); 

// compact the run-length stream [i, j], subdividing it in two substreams [i, m-1], [m, j].
size_t rl_stream_compaction_serial(rl_entry_t *input, size_t i, size_t m, size_t j);
cl_event rl_stream_compaction_cl(cl_command_queue que, cl_kernel rl_compact_cl_k, cl_mem d_input, cl_int numels, size_t wg_preferred, int entries_per_subarray, int n_wait_events, cl_event *wait_events);


#endif