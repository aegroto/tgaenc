#ifndef CONSOLE_CODES_H
#define CONSOLE_CODES_H

#define RESET            "\033[0m"
#define RED              "\033[31m"  
#define CYAN             "\033[36m"
#define GREEN            "\033[32m"
#define MAGENTA          "\033[35m"
#define BRIGHTCYAN       "\033[96m"
#define BRIGHTGREEN      "\033[92m"
#define BRIGHTMAGENTA    "\033[95m"
#define BOLD             "\033[1m"

#endif