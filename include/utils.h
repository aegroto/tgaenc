#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>

char* uchar_to_binstr(unsigned char input);
char* uint_to_binstr(unsigned int input);
unsigned int next_power_of_two(unsigned int v);

#endif