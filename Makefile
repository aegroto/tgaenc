CC=gcc
CFLAGS=-g -Wall -O3 -march=native 
LDLIBS=-lOpenCL -lm

INCFOLDER=include/
SRCFILES=src/*.c

build: 
	$(CC) -o out $(CFLAGS) $(SRCFILES) -I$(INCFOLDER) $(LDLIBS)

clean:
	rm out 
